package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter extends ArrayAdapter<String> {

         AnimalList animalList;

        public Adapter(Context context, String[] animal) {
            super(context, 0, animal);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            String animalname = getItem(position);

            if (view == null) {
                view = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            }

            final Animal animal = animalList.getAnimal(animalname);
            ImageView image =  view.findViewById(R.id.image);
            TextView animalName =  view.findViewById(R.id.animal);

            int id = view.getResources().getIdentifier("com.example.myapplication:drawable/" +animal.getImgFile() , null, null);
            image.setImageResource(id);
            animalName.setText(animalname);
            return view;
        }


}
