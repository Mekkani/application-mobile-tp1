package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {


    String animalSelectedName;
    Animal animalSelected;
    ImageView image;
    TextView animalName;
    TextView hightestLifespan;;
    TextView gestationPeriod;
    TextView birthWeight;
    TextView adultWeight;
    EditText conservationStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        image =  findViewById(R.id.imageView);
        animalName =  findViewById(R.id.animalName);
        hightestLifespan = findViewById(R.id.hightestLifespan);
        gestationPeriod = findViewById(R.id. gestationPeriod);
        birthWeight= findViewById(R.id.birthWeight);
        adultWeight = findViewById(R.id.adultWeight);
        conservationStatus = findViewById(R.id.conservationStatus);

        AnimalList listAnimal= new AnimalList();

        animalSelectedName = getIntent().getStringExtra("animal");
        animalSelected  = listAnimal.getAnimal(animalSelectedName);


        int id = getResources().getIdentifier("com.example.myapplication:drawable/" + animalSelected.getImgFile(), null, null);
        image.setImageResource(id);


        animalName.setText(animalSelectedName);
        hightestLifespan.setText(animalSelected.getStrHightestLifespan());
        gestationPeriod.setText(animalSelected.getStrGestationPeriod());
        birthWeight.setText(animalSelected.getStrBirthWeight());
        adultWeight.setText(String.valueOf(animalSelected.getStrAdultWeight()));
        conservationStatus.setText(String.valueOf(animalSelected.getConservationStatus()));


        Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    String newConservationStatus = conservationStatus.getText().toString();
                    if(!animalSelected.getConservationStatus().equals(newConservationStatus)){

                        animalSelected.setConservationStatus(newConservationStatus);
                    }

                finish();
            }
        });
    }
}
