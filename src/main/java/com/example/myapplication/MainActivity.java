package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AnimalList  anlst = new AnimalList();
        String[] animal = anlst.getNameArray();
        final ListView lstview = findViewById(R.id.lstview);


        final Adapter adapter = new Adapter(this, animal);
        lstview.setAdapter(adapter);

        lstview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                Intent myIntent = new Intent(view.getContext(),SecondActivity.class);
                String animalChoice = (String) (lstview.getItemAtPosition(position));
                myIntent.putExtra("animal",animalChoice);
                startActivity(myIntent);

            }
        });
    }
}

